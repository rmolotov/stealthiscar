﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Car : MonoBehaviour
{
    public float timeBonus;
    public int cost;
    NavMeshAgent agent;
    Animator anim;
    Transform t;
    private void Start()
    {
        t = GameManager.Instance().wayPoints[Random.Range(0, GameManager.Instance().wayPoints.Count)];
        
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(t.position);
        anim = GetComponentInChildren<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Player")
        {
            agent.isStopped = true;
            anim.Play("collect");
            GameManager.Instance().player.ChangeScore(cost);
            GameManager.Instance().AddTime(timeBonus);
            GetComponent<AudioSource>().Play();
            Destroy(gameObject, 1.0f);

        }


    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawIcon(t.position, "images.png");
    }
    private void Update()
    {
        if (Vector3.Distance(agent.destination, transform.position) < 2)
        {
            t = GameManager.Instance().wayPoints[Random.Range(0, GameManager.Instance().wayPoints.Count)];
            agent.SetDestination(t.position);
        }
    }
}
