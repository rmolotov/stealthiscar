﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    public GameObject[] prefabs;
    void Start()
    {
        int seed = Random.Range(0, 3);
        GameManager.Instance().LoadMap(transform);
        int x = Random.Range(0, transform.childCount);
        GameObject.Instantiate(prefabs[seed], transform.GetChild(x).position, transform.transform.GetChild(x).rotation);
    }
}
